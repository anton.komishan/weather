import React from "react";

export const Filter: React.FC = () => (
    <div className="filter">
        <span className="checkbox selected">облачно</span>
        <span className="checkbox">солнечно</span>
        <p className="custom-input">
            <label htmlFor="min-temperature">минимальная температура</label>
            <input id="min-temperature" type="number" />
        </p>
        <p className="custom-input">
            <label htmlFor="min-temperature">максимальная температура</label>
            <input id="max-temperature" type="number" />
        </p>
        <button>отфильтровать</button>
    </div>
);
