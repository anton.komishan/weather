import React from "react";
import {ForecastDay} from "./ForecastDay";
import ForecastItem from "../models/ForecastItem";
import {useMockData} from "../hooks/useMockData";

export const Forecast: React.FC = () => {
    const forecast: ForecastItem[] = useMockData();
    return (
        <div className="forecast">
            {forecast.slice(0, 7).map(x => <ForecastDay day={x.dayName} temperature={x.temperature} type={x.type} />)}
        </div>
    );
}