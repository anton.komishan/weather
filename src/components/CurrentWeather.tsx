import React from "react";

export const CurrentWeather: React.FC = () => (
    <div className="current-weather">
        <p className="temperature">17</p>
        <p className="meta">
            <span className="rainy">%77</span>
            <span className="humidity">%64</span>
        </p>
    </div>
);