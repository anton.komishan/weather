import React from "react";

interface Props {
    day: string;
    temperature: number;
    type: string;
}

export const ForecastDay: React.FC<Props> = ({day, temperature, type}) => (
    <div className={`day ${type}`}>
        <p>{day}</p>
        <span>{temperature}</span>
    </div>
);