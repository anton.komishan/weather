import React from "react";

export const WeatherHeader: React.FC = () => (
    <div className="head">
        <div className="icon rainy" />
        <div className="current-date"><p>вторник</p><span>6 апреля</span></div>
    </div>
);