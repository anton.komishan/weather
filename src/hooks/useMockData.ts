import ForecastItem from "../models/ForecastItem";
import forecastJson from "../mock-data/forecast.json";
import {useMemo} from "react";

export const useMockData = (): ForecastItem[] => {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const getForecast: () => ForecastItem[] = () => forecastJson.map((x): ForecastItem => ({
        ...x,
        dayName: days[new Date(x.day).getDay()]
    }))
    return useMemo(() => getForecast(), [])
}