export default interface ForecastItem {
    id: string;
    rain_probability: number;
    humidity: number;
    day: number;
    dayName: string;
    temperature: number;
    type: string;
}