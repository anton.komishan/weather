import {Filter} from './components/Filter';
import {WeatherHeader} from "./components/WeatherHeader";
import {CurrentWeather} from "./components/CurrentWeather";
import {Forecast} from "./components/Forecast";

export const App = () => (
    <main>
        <Filter />
        <WeatherHeader />
        <CurrentWeather />
        <Forecast />
    </main>
);
